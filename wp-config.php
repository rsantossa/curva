<?php
/** 
 * As configurações básicas do WordPress.
 *
 * Esse arquivo contém as seguintes configurações: configurações de MySQL, Prefixo de Tabelas,
 * Chaves secretas, Idioma do WordPress, e ABSPATH. Você pode encontrar mais informações
 * visitando {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. Você pode obter as configurações de MySQL de seu servidor de hospedagem.
 *
 * Esse arquivo é usado pelo script ed criação wp-config.php durante a
 * instalação. Você não precisa usar o site, você pode apenas salvar esse arquivo
 * como "wp-config.php" e preencher os valores.
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar essas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('WP_CACHE', true); //Added by WP-Cache Manager
define( 'WPCACHEHOME', 'C:\UniServer\www\Curva\wp-content\plugins\wp-super-cache/' ); //Added by WP-Cache Manager
define('DB_NAME', 'curva_db');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', 'root');

/** nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Conjunto de caracteres do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8');

/** O tipo de collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * Você pode alterá-las a qualquer momento para desvalidar quaisquer cookies existentes. Isto irá forçar todos os usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '~7aLDs9xa%(1gG9Jyr.HPwbbgDH}R,CgH!4&3FW/ES8w$ylb6phe#+(=)4R9H_0e');
define('SECURE_AUTH_KEY',  'V/+}fp.{F+yS8uyFG/odX$QRr(Lse4Hi+kJ,~xmW|<eYX,][hbXrJ~78Jsmywjp:');
define('LOGGED_IN_KEY',    '`,:/-b-=6az>s@[hQ2Dg G*.W#e=KUn_?e}m{kS[s%5e(|(Rz;X?A&w5F0um4qdl');
define('NONCE_KEY',        '2nZYK]4cyCEr7KQbWbg0fSg,A.U(m78[t/S:4?iXGY><YQZs@@MvsL`h: <ct6q=');
define('AUTH_SALT',        'n3vh`N]{~>GR;.]5XD>cWQO|k~#0bW eL4EFR`<fpD@MkRznv/RB}]Na mLd1(l5');
define('SECURE_AUTH_SALT', 'G]Ixr:Qfmj)PXna=sB@KU;U=^$]SZFI!m{-bo%uDn-{v8rV(Ejvv=hCg6$}I&FqK');
define('LOGGED_IN_SALT',   'icuxbXaXL! trntwcLQn=!buxg3^<X&W;AJ9{!r7Jl0 St=|!TOQ!_=.raIvvsJ5');
define('NONCE_SALT',       'S$:VqD?[1myyoG9.{kCJX*_{N#]@%kcmxkkw/LAu)Hd[bv2!]a@Af<dAocBd^GIg');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der para cada um um único
 * prefixo. Somente números, letras e sublinhados!
 */
$table_prefix  = 'wp_';

/**
 * O idioma localizado do WordPress é o inglês por padrão.
 *
 * Altere esta definição para localizar o WordPress. Um arquivo MO correspondente ao
 * idioma escolhido deve ser instalado em wp-content/languages. Por exemplo, instale
 * pt_BR.mo em wp-content/languages e altere WPLANG para 'pt_BR' para habilitar o suporte
 * ao português do Brasil.
 */
define('WPLANG', 'pt_BR');

/**
 * Para desenvolvedores: Modo debugging WordPress.
 *
 * altere isto para true para ativar a exibição de avisos durante o desenvolvimento.
 * é altamente recomendável que os desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');
	
/** Configura as variáveis do WordPress e arquivos inclusos. */
require_once(ABSPATH . 'wp-settings.php');
