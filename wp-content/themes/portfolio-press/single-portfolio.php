<?php
/**
 * Template for displaying a portfolio post
 *
 * @package Portfolio Press
 */

get_header(); ?>

	<div id="primary">
		<div id="content" role="main">

		<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<header class="entry-header">
					<h1 class="entry-title"><?php the_title(); ?></h1>

				</header><!-- .entry-header -->

				<div class="entry-content">
                
                <?php if ( has_post_thumbnail() && of_get_option('portfolio_images', "1") ) {
                	if ( of_get_option( 'layout') == 'layout-1col' ) {
	                	the_post_thumbnail( 'portfolio-fullwidth' );
                	} else {
	                	the_post_thumbnail( 'portfolio-large' );
                	}
				}
				?>
                
					<?php the_content(); ?>
					<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'portfoliopress' ), 'after' => '</div>' ) ); ?>
				</div><!-- .entry-content -->

				
			</article><!-- #post-<?php the_ID(); ?> -->
			
			

			<?php if ( comments_open() ) {
				comments_template( '', true );
            } ?>

		<?php endwhile; // end of the loop. ?>

		</div><!-- #content -->
	</div><!-- #primary -->
	<script type="text/javascript">
	jQuery(function () {
		jQuery(window).on('slideshowInit', function() {
			if (jQuery('.slideshow_pagination li').length == 1) {
				jQuery('.slideshow_pagination, .slideshow_button').hide();
			} else {
				jQuery('.portfolio .slideshow_container').addClass('with-buttons');
			}
		});
	})
	</script>
<?php get_sidebar(); ?>
<?php get_footer(); ?>