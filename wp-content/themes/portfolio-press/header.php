<?php
/**
 * Header template
 *
 * @package Portfolio Press
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link href="http://fonts.googleapis.com/css?family=Roboto+Slab:400,700,300,100" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_stylesheet_uri(); ?>" />
<?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); ?>
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
<![endif]-->
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="wrapper">
	<header id="branding">        
		<hgroup id="logo">
			<h1 id="site-title">
				<a href="<?php echo home_url( '/' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
	            <?php if ( of_get_option('logo', false) ) { ?>
					<img src="<?php echo of_get_option('logo'); ?>" alt="<?php echo bloginfo( 'name' ) ?>" />
				<?php } else {
					bloginfo( 'name' );
				}?></a>
            </h1>
			<?php if ( !of_get_option('logo', false) ) { ?>
            	<h2 id="site-description"><?php bloginfo( 'description' ); ?></h2>
            <?php } ?>
		</hgroup>
      
		<nav id="navigation">
			<h3 class="menu-toggle"><?php _e( 'Menu', 'portfoliopress' ); ?></h3>
			<?php wp_nav_menu( array( 'theme_location' => 'primary') ); ?>
		</nav><!-- #access -->    
	    <span class="bar bar-1"></span>
	    <span class="bar bar-2"></span>
	    <span class="bar bar-3"></span>
	    <span class="bar bar-4"></span>
	    <span class="big-horizontal-bar"></span>
	</header><!-- #branding -->

	<div id="main">